export const expectedOutput = {
    records: [
        {
          "keys": [
            "n"
          ],
          "length": 1,
          "_fields": [
            {
              "identity": {
                "low": 0,
                "high": 0
              },
              "labels": [
                "User"
              ],
              "properties": {
                "password": "password",
                "id": "58b15b96-a9b4-40e3-950a-7c9a945360cc",
                "fullname": "Jess",
                "email": "Jess@yahoo.com"
              }
            }
          ],
          "_fieldLookup": {
            "n": 0
          }
        },
        {
          "keys": [
            "n"
          ],
          "length": 1,
          "_fields": [
            {
              "identity": {
                "low": 1,
                "high": 0
              },
              "labels": [
                "User"
              ],
              "properties": {
                "password": "password",
                "id": "30ac127e-c28d-40cb-baa2-a1e28d54e5af",
                "fullname": "Mark",
                "email": "Mark@yahoo.com"
              }
            }
          ],
          "_fieldLookup": {
            "n": 0
          }
        },
        {
          "keys": [
            "n"
          ],
          "length": 1,
          "_fields": [
            {
              "identity": {
                "low": 2,
                "high": 0
              },
              "labels": [
                "User"
              ],
              "properties": {
                "password": "password",
                "id": "304587cb-a701-43d7-a78d-f709886e6747",
                "fullname": "Tselmeg",
                "email": "Tselmeg@yahoo.com"
              }
            }
          ],
          "_fieldLookup": {
            "n": 0
          }
        },
        {
          "keys": [
            "n"
          ],
          "length": 1,
          "_fields": [
            {
              "identity": {
                "low": 3,
                "high": 0
              },
              "labels": [
                "User"
              ],
              "properties": {
                "password": "password",
                "id": "41ad2218-a286-4936-8229-044ae9d7a908",
                "fullname": "Jack",
                "email": "Jack@yahoo.com"
              }
            }
          ],
          "_fieldLookup": {
            "n": 0
          }
        }
      ]
      
}