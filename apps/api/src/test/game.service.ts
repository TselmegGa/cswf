import { FilterQuery, Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { GameDto } from '../app/game/dto/game.dto';
import { Game } from '../app/game/schemas/game.schema';

@Injectable()
export class mockGamesService {
  constructor() {}

  async create(gameDto: GameDto): Promise<Game> {

    return undefined
  }

  async findAll(): Promise<Game[]> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve([{
          name: "Call of Door",
          genre: "Shooter",
          publisher: "Freeze inc."
        }]);
      }, 300);
    });
  }
  async findById(id): Promise<Game> {
    return undefined
  }

  async update(id, gameDto: GameDto): Promise<Game> {
    return undefined
  }
  async delete(id): Promise<Game> {
    return undefined
  }
}
