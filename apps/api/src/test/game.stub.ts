import { Game } from "../app/game/schemas/game.schema"

export const gameStub = (): Game => {
 return {
     name: "Test game",
     genre: "shooter",
     publisher: "Avans"
  }
}