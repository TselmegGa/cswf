import { Game } from "../app/game/schemas/game.schema";


import { MockModel } from "./mock.model";
import { gameStub } from "./game.stub";

export class GameModel extends MockModel<Game> {
  protected entityStub = gameStub()
}