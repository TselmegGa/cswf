import { Node } from 'neo4j-driver'

export class Player {

    constructor(private readonly node: Node) {}

    getId(): string {
        return (<Record<string, any>> this.node.properties).id
    }

    toJson(): Record<string, any> {
        return <Record<string, any>> this.node.properties;

    }
}