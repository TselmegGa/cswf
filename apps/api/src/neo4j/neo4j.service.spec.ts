import { Test, TestingModule } from '@nestjs/testing';
import { AppService } from '../app/app.service';
import {  Neo4jService } from './neo4j.service';
import {  Neo4jModule } from './neo4j.module';
import { User } from '../neo4j/user.entity';
import { UserDto } from '../app/user/dto/user.dto';


jest.mock('neo4j-driver/lib/driver')

import { mockNode, mockResult } from 'nest-neo4j/dist/test'
describe('ArticleService', () => {
  let service: AppService;
  let neo4jService: Neo4jService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        Neo4jModule.forRoot({
          scheme: 'neo4j+s',
          host: 'f395ecc0.databases.neo4j.io',
          port: 7687,
          username: 'neo4j',
          password: 'JsA7K45EhRzZY5V0iM7_hg6uhQF1tL3_xpoWdNaQDGM',
          database: "test"
      
        },)
        
      ],
      providers: [AppService],
    }).compile();

    service = await module.resolve<AppService>(AppService);
    neo4jService = module.get(Neo4jService)
  });

 
    describe('::create()', () => {
      it('should create a new article', async () => {
        expect(service).toBeDefined();
        expect(neo4jService).toBeDefined();
  
        const data = {
          
            fullname: "Bob",
          password: "password",
          email: "thomas@bob.com"
          
        }

  
        /* Assign user to request
        const user = new User( mockNode('User', { id: 'test-user' } ) )
        Object.defineProperty(service, 'request', { value: { user } })
  
        // Mock the response from neo4jService.write
        const write = jest.spyOn(neo4jService, 'write')
          .mockResolvedValue(
            mockResult([
              {
                u: user,
                a: mockNode('User', { ...data, id: 'test-user-1' }),

              },
            ])
          )
        const article = await service.addUserNeo(data)
  
        const json = article.toJson()
  
        expect(json).toEqual({
          ...data,
          author: user.toJson(),
          id: 'test-user-1',
        })*/
  
      })
    })
  })
