import { Node } from 'neo4j-driver'

export class User {

    constructor(private readonly node: Node) {}

    getId(): string {
        return (<Record<string, any>> this.node.properties).id
    }

    getPassword(): string {
        return (<Record<string, any>> this.node.properties).password
    }

    getClaims() {
        const { fullname, email,  } = <Record<string, any>> this.node.properties

        return {
            sub: fullname,
            fullname,
            email,

        }
    }

    toJson(): Record<string, any> {
        const { password, ...properties } = <Record<string, any>> this.node.properties;

        return {
            ...properties,
        }
    }
}