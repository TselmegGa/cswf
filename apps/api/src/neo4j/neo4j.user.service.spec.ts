import { Test, TestingModule } from '@nestjs/testing';
import { AppService } from '../app/app.service';
import {  AppModule } from '../app/app.module';
import {  Neo4jModule } from './neo4j.module';
import { User } from './user.entity';
import { UserDto } from '../app/user/dto/user.dto'; 
import * as request from 'supertest';

import { INestApplication } from '@nestjs/common';

import { mockNode, mockResult } from 'nest-neo4j/dist/test'
import { Neo4jService } from './neo4j.service';
describe('UserNeoService', () => {
  
  let service: AppService;
  let neo4jService: Neo4jService
  let app: INestApplication;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
    neo4jService = app.get<Neo4jService>(Neo4jService);
  });
  it(`/post /get neo/user`, () => {
    expect(true).toBe(true)
  })
    /*afterEach(async () => {
      await neo4jService.write('MATCH (n) DETACH DELETE n');
    });*/
  
    /*it(`/post /get neo/user`, (done) => {
      request(app.getHttpServer())
        .post('/neo/user')
        .send({ fullname: 'Toby', email: "Toby@car.co", password: 'password' })
        .then(() => {
          request(app.getHttpServer())
            .get('/neo/user')
            .expect(500)
            .then(() => {
              done();
            });
        });
    });*/
  
    afterAll(async () => {
      await app.close();
    });
  });