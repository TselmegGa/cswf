import { Injectable } from '@nestjs/common';
import { QueryResult } from 'neo4j-driver';
import { Player } from '../neo4j/player.entity';
import { Neo4jService } from '../neo4j/neo4j.service';



@Injectable()
export class NeoPlayerService {
  constructor(private readonly neo4jService: Neo4jService){}

  async getAllPlayers(): Promise<QueryResult | undefined>{
    return this.neo4jService.read(`MATCH (n:Player) RETURN n`).then(res => {
    
      if ( res.records.length == 0 ) return undefined
     return res

})  
 }
 async getPlayerByIdNeo(id:string){
  return this.neo4jService.read(`MATCH (n:Player) WHERE n.id = $id RETURN n, $id`,{id}).then(res => {
  
    if ( res.records.length == 0 ) return undefined
   return res.records[0].get('n');

})  
}
 async addPlayerNeo(id:any){
  return this.neo4jService.write(
    `create (n:Player {id: $id}) RETURN n `,{id}
    ).then(res => {
    
      if ( res.records.length == 0 ) return undefined
     return res.records[0].get('n');

})
}

async deletePlayerNeo(id:string){
  return this.neo4jService.write(
    `MATCH (n:Player) WHERE n.id = $id DELETE n`,{id}
    ).then(res => {
      if ( res.records.length == 0 ) return undefined
      return res.records[0].get('n');
    
  })
}
async getAllLiked(id:string){
  return this.neo4jService.read(`MATCH (follower:User)-[r:LIKES]->(target:Player) WHERE target.id = $id RETURN follower`,{id}).then(res => {
  
    if ( res.records.length == 0 ) return undefined
   return res;

}) 
}
async getAllLikes(id:string){
  return this.neo4jService.read(`MATCH (follower:User)-[r:LIKES]->(target:Player) WHERE follower.id = $id RETURN target`,{id}).then(res => {
  
    if ( res.records.length == 0 ) return undefined
   return res;

}) 
}
async isLiked(targetId: string, followingId: string){
    return this.neo4jService.read(`
        MATCH (target:Player)<-[:LIKES]-(follower:User)
        WHERE target.id = $targetId AND follower.id = $followingId
        RETURN count(*) AS count
    `, {
        targetId,
        followingId,
    })
        .then(res => {
            return res.records[0].get('count') > 0
        })
}

like(targetId: string, followingId: string){
    return this.neo4jService.write(`
        MATCH (target:Player)
        MATCH (follower:User)
        WHERE target.id = $targetId AND follower.id = $followingId
        MERGE (follower)-[r:LIKES]->(target)
        ON CREATE SET r.createdAt = datetime()
        RETURN target
    `, {
      targetId,
      followingId,
  })
        .then(res => {
            if ( res.records.length == 0 ) return undefined

            return new Player(res.records[0].get('target'))
        })
}

unlike(targetId: string, followingId: string){
    return this.neo4jService.write(`
        MATCH (target:Player)<-[r:LIKES]-(follower:User)
        WHERE target.id = $targetId AND follower.id = $followingId
        DELETE r
        RETURN target
    `, {
      targetId,
      followingId,
  })
        .then(res => {
            if ( res.records.length == 0 ) return undefined

            return new Player(res.records[0].get('target'))
        })
    }

}
