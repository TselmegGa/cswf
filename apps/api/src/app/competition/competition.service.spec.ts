import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';
import { NestExpressApplication } from '@nestjs/platform-express';
import { Test } from '@nestjs/testing';
import { Connection } from 'mongoose';
import * as supertest from 'supertest';
import { CompetitionsModule } from './competition.module';
import { CompetitionsService } from './competition.service';
import { Competition, CompetitionSchema } from './schemas/competition.schema';

const expectedCompetition: Competition[] = [
  {
    name: "RTSunday 2020",
    game: {  name: 'Red Alert 4', genre: 'RTS', publisher: 'EA' },
    winner: {
    
      name: 'Stabby',
      birthday:  new Date('1995-12-17T03:24:00'),
      bio: 'Mark "Stabby" Johnson is a professional RTS player. He plays Red alert 4, Red alert 3 and They are billions.'
    },
    description: "RTSunday 2020 was a competition organized on January 12 2020. This competition had a grandprice of $30,000.",
    players: [{
    
      name: 'Stabby',
      birthday:  new Date('1995-12-17T03:24:00'),
      bio: 'Anatoliy "Stabby" Ranjeet is a professional RTS player. He plays Red alert 4, Red alert 3 and They are billions.'
    }]
  },
  {
    name: "RTSunday 2022",
    game: {  name: 'Red Alert 4', genre: 'RTS', publisher: 'EA' },
    winner: {
    
      name: 'Stabby',
      birthday:  new Date('1995-12-17T03:24:00'),
      bio: 'Mark "Stabby" Johnson is a professional RTS player. He plays Red alert 4, Red alert 3 and They are billions.'
    },
    description: "RTSunday 2020 was a competition organized on January 12 2020. This competition had a grandprice of $30,000.",
    players: [{
    
      name: 'Stabby',
      birthday:  new Date('1995-12-17T03:24:00'),
      bio: 'Anatoliy "Stabby" Ranjeet is a professional RTS player. He plays Red alert 4, Red alert 3 and They are billions.'
    }]
  },
];

describe('CompetitionController', () => {
  let service: CompetitionsService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot('mongodb+srv://Tselmeg:Tselpass@cluster0.txqktxh.mongodb.net/?retryWrites=true&w=majority',
        { dbName: 'test',useNewUrlParser: true, useUnifiedTopology: true }), // we use Mongoose here, but you can also use TypeORM
        CompetitionsModule,
        MongooseModule.forFeature([{ name: Competition.name, schema: CompetitionSchema }])
      ], providers: [
        CompetitionsService]
    }).compile();

    service = moduleRef.get<CompetitionsService>(CompetitionsService);
  });


  it('should be true', () => {
    expect(true).toEqual(true);
  });
  
  it('creates a Competition', async () => {
    const created = await service.create(expectedCompetition[0])
    const searched = await service.findById(created._id)
    expect(created.name).toEqual(searched.name)
  })
  it('deletes a Competition', async () => {
    const created = await service.create(expectedCompetition[0])
    await service.delete(created.id)
    const searched = await service.findById(created._id)
    expect(searched).toBeNull()
  })
  it('updates a Competition', async () => {
    const created = await service.create(expectedCompetition[0])
    const updated = await service.update(created.id, expectedCompetition[1])
    const searched = await service.findById(created._id)
    expect(updated.name).toEqual(searched.name)
  })
  it('gets all Competition', async () => {
    const created = await service.create(expectedCompetition[0])
    const searched = await service.findAll()
    expect(searched.length).toBeGreaterThanOrEqual(1)
  })
});