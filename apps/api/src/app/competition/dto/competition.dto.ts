import { Game } from '../../game/schemas/game.schema';
import { Player } from '../../player/schemas/player.schema';

export class CompetitionDto {
  readonly name: string;
  readonly game: Game;
  readonly players: [Player];
  readonly winner: Player;
  readonly description: string;
}
