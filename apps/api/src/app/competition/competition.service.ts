import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Competition, CompetitionDocument } from './schemas/competition.schema';
import { CompetitionDto } from './dto/competition.dto';

@Injectable()
export class CompetitionsService {
  constructor(@InjectModel(Competition.name) private competitionModel: Model<CompetitionDocument>) {}

  async create(competitionDto: CompetitionDto): Promise<any> {
    const createdPlayer = new this.competitionModel(competitionDto);
    return await createdPlayer.save();
  }

  async findAll(): Promise<Competition[]> {
    return await this.competitionModel.find().exec();
  }
  
  async findById(id): Promise<Competition> {
    const customer = await this.competitionModel.findById(id).exec();
    return customer;
  }
  async findByGame(id): Promise<Competition[]> {
    return await this.competitionModel.find({'game._id': id}).exec();
  }

  async update(id, competitionDto: CompetitionDto): Promise<Competition> {
    return await this.competitionModel.findByIdAndUpdate(id, competitionDto, { new: true });
  }
  async delete(id): Promise<Competition> {
    return await this.competitionModel.findByIdAndRemove(id);
  }
}
