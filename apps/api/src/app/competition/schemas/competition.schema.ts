import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Game } from '../../game/schemas/game.schema';
import { Player } from '../../player/schemas/player.schema';

export type CompetitionDocument = Competition & Document;

@Schema()
export class Competition {
  @Prop({type: String, required: true })
  name: string;

  @Prop({type: Game, required: true })
  game: Game;

  @Prop({type: Player})
  players: [Player];

  @Prop({type: Player})
  winner: Player;

  @Prop({type: String})
  description: string;
}

export const CompetitionSchema = SchemaFactory.createForClass(Competition);
CompetitionSchema.post('save', function(doc) {
  console.log('%s has been saved', doc._id);
});

