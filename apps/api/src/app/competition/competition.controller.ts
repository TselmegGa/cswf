import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { CompetitionsService } from './competition.service';
import { CompetitionDto } from './dto/competition.dto';
import { Competition } from './schemas/competition.schema';
import { JwtAuthGuard } from '../auth/jwt-auth';

@Controller('competitions')
export class CompetitionsController {
  constructor(private readonly competitionsService: CompetitionsService) {}
  @UseGuards(JwtAuthGuard)
  // I used embedding to make sure that the conditions of the game and the players of the competition shows the state of current time
  @Post()
  async create(@Body() competitionDto: CompetitionDto) {
    return await this.competitionsService.create(competitionDto);
    //res.status(200).send("Registered.");
  }

  @Get()
  async findAll(){
    return this.competitionsService.findAll();
  }
  @Get(':id')
  async findById(@Param('id') id: string){
    return this.competitionsService.findById(id);
  }
  @Get('game/:id')
  async findByGame(@Param('id') id: string) {
    return this.competitionsService.findByGame(id);
  }
  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async update(@Param('id') id: string, @Body() competitionDto: CompetitionDto){
    return this.competitionsService.update(id, competitionDto);
  }
  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<Competition> {
    return this.competitionsService.delete(id);
  }

}
