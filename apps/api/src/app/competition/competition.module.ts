import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CompetitionsController } from './competition.controller';
import { CompetitionsService } from './competition.service';
import { Competition, CompetitionSchema } from './schemas/competition.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: Competition.name, schema: CompetitionSchema }])],
  controllers: [CompetitionsController],
  providers: [CompetitionsService],
})
export class CompetitionsModule {}
