import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CompetitionsModule } from './competition/competition.module';
import { GamesModule } from './game/game.module';
import { PlayersModule } from './player/player.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { AuthModule } from './auth/auth.module';
import { join } from 'path';
import { UsersModule } from './user/user.module';
import { Neo4jModule } from '../neo4j/neo4j.module';
import { NeoPlayerService } from './neo4j.player.service';
import { PlayersService } from './player/player.service';

@Module({
  imports: [MongooseModule.forRoot("mongodb+srv://Tselmeg:Tselpass@cluster0.txqktxh.mongodb.net/?retryWrites=true&w=majority",
   {dbName: 'GOG',useNewUrlParser: true, useUnifiedTopology: true}),
  Neo4jModule.forRoot({
    scheme: 'neo4j+s',
    host: 'f395ecc0.databases.neo4j.io',
    port: 7687,
    username: 'neo4j',
    password: 'JsA7K45EhRzZY5V0iM7_hg6uhQF1tL3_xpoWdNaQDGM'

  }),
  AuthModule,
  PlayersModule,
  CompetitionsModule,
  GamesModule,
  UsersModule,
  ServeStaticModule.forRoot({
    rootPath: join(__dirname, '..', 'angular-demo'),
  }),],
  controllers: [AppController],
  providers: [AppService, NeoPlayerService],
})
export class AppModule {}
