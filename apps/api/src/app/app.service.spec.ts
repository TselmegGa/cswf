import neo4j, { Driver } from 'neo4j-driver';

describe('Verify Neo4j Connectivity', () => {
  let driver: Driver;

  beforeAll(async () => {
    driver = neo4j.driver(
      `neo4j+s://f395ecc0.databases.neo4j.io:7687`,
      neo4j.auth.basic('neo4j', 'JsA7K45EhRzZY5V0iM7_hg6uhQF1tL3_xpoWdNaQDGM'),
    );
  });

  afterAll(async () => {
    await driver.close();
  });

  it('should verify connectivity', async () => {
    expect(await driver.verifyConnectivity()).toMatchInlineSnapshot(`
      Object {
        "address": "f395ecc0.databases.neo4j.io:7687",
        "version": "Neo4j/5.2-aura",
      }
    `);
  });
});