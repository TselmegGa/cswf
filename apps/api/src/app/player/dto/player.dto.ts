export class PlayerDto {
  readonly name: string;
  readonly birthday: Date;
  readonly bio: string;
}
export class PlayerTest {
  name: string;
  birthday: Date;
  bio: string;
}
