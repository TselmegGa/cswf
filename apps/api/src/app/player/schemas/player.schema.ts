import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PlayerDocument = Player & Document;

@Schema()
export class Player {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: Date, required: true })
  birthday: Date;

  @Prop( {type: String})
  bio: string;
}

export const PlayerSchema = SchemaFactory.createForClass(Player);
PlayerSchema.post('save', function(doc) {
  console.log('%s has been saved', doc._id);
});