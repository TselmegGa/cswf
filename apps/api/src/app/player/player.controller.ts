import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { PlayersService } from './player.service';
import { PlayerDto } from './dto/player.dto';
import { Player } from './schemas/player.schema';

@Controller('players')
export class PlayersController {
  constructor(private readonly playersService: PlayersService) {}

  @Get()
  async findAll(){
    return this.playersService.findAll();
  }
  @Get(':id')
  async findById(@Param('id') id: string){
    return this.playersService.findById(id);
  }
  @Put(':id')
  async update(@Param('id') id, @Body() playerDto: PlayerDto) {
    return this.playersService.update(id, playerDto);
  }
  @Delete(':id')
  async remove(@Param('id') id: string) {
    return this.playersService.delete(id);
  }

}
