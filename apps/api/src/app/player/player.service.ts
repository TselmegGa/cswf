import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Player, PlayerDocument } from './schemas/player.schema';
import { PlayerDto } from './dto/player.dto';
@Injectable()
export class PlayersService {
  constructor(@InjectModel(Player.name) private playerModel: Model<PlayerDocument>) {}


  async create(playerDto: PlayerDto): Promise<any> {
    const createdPlayer = new this.playerModel(playerDto);
    return await createdPlayer.save();
  }

  async findAll(): Promise<Player[]> {
    return await this.playerModel.find().exec();
  }
  async findById(id): Promise<Player> {
    const customer = await this.playerModel.findById(id).exec();
    return customer;
  }

  async update(id, playerDto: PlayerDto): Promise<Player> {
    return await this.playerModel.findByIdAndUpdate(id, playerDto, { new: true });
  }
  async delete(id): Promise<Player> {
    return await this.playerModel.findByIdAndRemove(id);
  }
}
