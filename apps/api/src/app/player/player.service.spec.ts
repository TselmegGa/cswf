import { Test } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import { PlayersService } from './player.service';
import { Player, PlayerSchema } from './schemas/player.schema';
import { PlayersModule } from './player.module';

const expectedPlayers:Player[] =  [{
  name: 'Johnson',
  bio:'Johnson is nice. :)',
  birthday: new Date()
},
{
  name: 'Jack',
  bio:'Jack is mean. :(',
  birthday: new Date()
}]
describe('PlayersService', () => {
  let service: PlayersService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot('mongodb+srv://Tselmeg:Tselpass@cluster0.txqktxh.mongodb.net/?retryWrites=true&w=majority',
        { dbName: 'test',useNewUrlParser: true, useUnifiedTopology: true }), // we use Mongoose here, but you can also use TypeORM
        PlayersModule,
        MongooseModule.forFeature([{ name: Player.name, schema: PlayerSchema }])
      ], providers: [
        PlayersService]
    }).compile();

    service = moduleRef.get<PlayersService>(PlayersService);
  });


  it('should be true', () => {
    expect(true).toEqual(true);
  });
  
  it('creates a Player', async () => {
    return await service.create(expectedPlayers[0]).then( async (created)=>{
      await service.findById(created._id).then((searched)=>{
        expect(created.name).toEqual(searched.name)
      })
    })

   
  })
  it('deletes a Player', async () => {
    return await service.create(expectedPlayers[0]).then( async (created)=>{
      await service.delete(created.id).then( async ()=>{
        await service.findById(created._id).then( (searched)=>{
          expect(searched).toBeNull()
        })
      })
    })
      
  })
  it('updates a Player', async () => {
    return await service.create(expectedPlayers[0]).then( async (created)=>{
    await service.update(created.id, expectedPlayers[1]).then( async (updated)=>{
      await service.findById(created._id).then( (searched)=>{
        expect(updated.name).toEqual(searched.name)
      })
    })
  })
  })
  it('gets all Player', async () => {
    return await service.create(expectedPlayers[0]).then( async ()=>{
    await service.findAll().then( (searched)=>{
    expect(searched.length).toBeGreaterThanOrEqual(1)
    })
  })
  })
});
