import { Injectable } from '@nestjs/common';
import { QueryResult } from 'neo4j-driver';
import { Neo4jService } from '../neo4j/neo4j.service';
import { User } from '../neo4j/user.entity';
import { UserDto } from './user/dto/user.dto';


@Injectable()
export class AppService {
  constructor(private readonly neo4jService: Neo4jService){}

  async getAll(){
    return this.neo4jService.read(`MATCH (n:User) RETURN n`).then(res => {
    
      if ( res.records.length == 0 ) return undefined
     return res

})  
  }
  async getByEmailNeo(email:string){
    return this.neo4jService.read(`MATCH (n:User) WHERE n.email = $email RETURN n, $email`,{email}).then(res => {
      if ( res.records.length == 0 ) return undefined
     return res.records[0].get('n');

})  
 }
 async getByIdNeo(id:string){
  return this.neo4jService.read(`MATCH (n:User) WHERE n.id = $id RETURN n, $id`,{id}).then(res => {
  
    if ( res.records.length == 0 ) return undefined
   return res.records[0].get('n');

})  
}
 async addUserNeo(user:any, id:any){
  console.log(id)
  console.log(user)
  return this.neo4jService.write(
    `create (n:User {id: $id, fullname : $fullname, password : $password, email : $email}) RETURN n `,
    {
      id: id,
      fullname: user.fullname,
      password: user.password,
      email: user.email

    }
    ).then(res => {
    
      if ( res.records.length == 0 ) return undefined
     return res.records[0].get('n');

})
}

async deleteUserNeo(id:string){
  return this.neo4jService.write(
    `MATCH (n:User) WHERE n.id = $id DELETE n`,{id}
    ).then(res => {
      if ( res.records.length == 0 ) return undefined
      return res.records[0].get('n');
    
  })
}
async updateUserNeo(id:string, user:UserDto){
  return this.neo4jService.write(
    `
            MATCH (n:User)
            WHERE n.id = $id
            SET n.updatedAt = localdatetime(), n += $updates
            RETURN n
        `, { id,updates: user }).then(res => {
    
          if ( res.records.length == 0 ) return undefined
         return res.records[0].get('n');

  })
}
async getAllFollowing(id:string){
  return this.neo4jService.read(`MATCH (follower:User)-[r:FOLLOWS]->(target:User) WHERE target.id = $id RETURN follower`,{id}).then(res => {
  
    if ( res.records.length == 0 ) return undefined
   return res;

}) 
}
async getAllFollows(id:string){
  return this.neo4jService.read(`MATCH (follower:User)-[r:FOLLOWS]->(target:User) WHERE follower.id = $id RETURN target`,{id}).then(res => {
  
    if ( res.records.length == 0 ) return undefined
   return res;

}) 
}
async isFollowing(targetId: string, followingId: string){
    return this.neo4jService.read(`
        MATCH (target:User)<-[:FOLLOWS]-(follower:User)
        WHERE target.id = $targetId AND follower.id = $followingId
        RETURN count(*) AS count
    `, {
        targetId,
        followingId,
    })
        .then(res => {
            return res.records[0].get('count') > 0
        })
}

follow(targetId: string, followingId: string){
    return this.neo4jService.write(`
        MATCH (target:User)
        MATCH (follower:User)
        WHERE target.id = $targetId AND follower.id = $followingId
        MERGE (follower)-[r:FOLLOWS]->(target)
        ON CREATE SET r.createdAt = datetime()
        RETURN target
    `, {
      targetId,
      followingId,
  })
        .then(res => {
            if ( res.records.length == 0 ) return undefined

            return new User(res.records[0].get('target'))
        })
}

unfollow(targetId: string, followingId: string){
    return this.neo4jService.write(`
        MATCH (target:User)<-[r:FOLLOWS]-(follower:User)
        WHERE target.id = $targetId AND follower.id = $followingId
        DELETE r
        RETURN target
    `, {
      targetId,
      followingId,
  })
        .then(res => {
            if ( res.records.length == 0 ) return undefined

            return new User(res.records[0].get('target'))
        })
    }

}
