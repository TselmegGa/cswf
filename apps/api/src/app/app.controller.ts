import { Body, Controller, Delete, Get, Param, Post, Put, Req, UseGuards } from '@nestjs/common';
import { AuthService } from './auth/AuthService';
import { AppService } from './app.service';

import { UserDto } from './user/dto/user.dto';
import { JwtAuthGuard } from './auth/jwt-auth';
import { User } from '../neo4j/user.entity';
import { QueryResult } from 'neo4j-driver';
import { UsersService } from './user/user.service';
import { NeoPlayerService } from './neo4j.player.service';
import { PlayersService } from './player/player.service';
import { PlayerDto } from './player/dto/player.dto';
@Controller()
export class AppController {
  constructor(private readonly appService: AppService,
    private readonly playerService: NeoPlayerService,
    private readonly usersService: UsersService,
    private readonly mongoplayersService: PlayersService,
    private readonly authService: AuthService) {}

  @UseGuards(JwtAuthGuard)
  @Get('neo/users/current')
  async findUserByEmailNeo(@Req() req){
    return (await this.appService.getByEmailNeo(req.user.username)).records;
  }
  @Get('neo/users/:id')
  async findUserByIdNeo(@Param('id') id: string){
    const m = await this.appService.getByIdNeo(id);
    if (m === null) {
      throw new Error('Unknown user')
    }
    return m.records;
  }
  @Get('neo/users')
  async getAllUsers(){
    return (await this.appService.getAll()).records;
  }
  @Get('neo/users/follow/:id')
  async getAllFollowing(@Param('id') id: string){
    const m = await this.appService.getAllFollowing(id);
    if (m === null) {
      throw new Error('Unknown user')
    }
    return m.records;
  }
  @Get('neo/users/followed/:id')
  async getAllFollows(@Param('id') id: string){
    const m = await this.appService.getAllFollows(id);
    if (m === null) {
      throw new Error('Unknown user')
    }
    return m.records;
  }
  // Using Promise.all is faster than using await multiple times
  // Promise.all will fail after the first fail
  // Await will fail after all of them failed
  @Post('users')
  async addUserNeo(@Body() userDto: UserDto) {
    
    const user = await this.usersService.create(userDto);
    return this.appService.addUserNeo(userDto, String(user._id));

  }
  @Post('players')
  async addPlayerNeo(@Body() playerDto: PlayerDto){
    const user = await this.mongoplayersService.create(playerDto);
    return this.playerService.addPlayerNeo(String(user._id));

  }
  @Post('neo/users/follow')
  async follow(@Body() body){
    return this.appService.follow(body.target, body.following);
  }
  @Put('users/:id')
  async updateUserNeo(@Param('id') id: string,@Body() userDto: UserDto){
    await this.usersService.update(id, userDto);
    return (await this.appService.updateUserNeo(id, userDto)).records;;
  }
  @Delete('users/:id')
  async deleteUserNeo(@Param('id') id: string) {
    await this.usersService.delete(id);
    return (await this.appService.deleteUserNeo(id)).records;
  }
  @Post('neo/users/unfollow')
  async unfollow(@Body() body) {
    return this.appService.unfollow(body.target, body.following);
  }
  @Post('/login')
  async login(@Body() userDto: UserDto) {
    return this.authService.login(userDto);
  }
  @Get('neo/users/liked/:id')
  async getAllLiked(@Param('id') id: string) {
    return (await this.playerService.getAllLiked(id)).records;
  }
  @Get('neo/users/likes/:id')
  async getAllLikes(@Param('id') id: string){
    return (await this.playerService.getAllLikes(id)).records;
  }
  @Post('neo/users/like')
  async like(@Body() body){
    return this.playerService.like(body.target, body.following);
  }
  @Post('neo/users/unlike')
  async unlike(@Body() body){
    return this.playerService.unlike(body.target, body.following);
  }
}
