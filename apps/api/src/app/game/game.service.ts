import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Game, GameDocument } from './schemas/game.schema';
import { GameDto } from './dto/game.dto';

@Injectable()
export class GamesService {
  constructor(@InjectModel("Game") private gameModel: typeof Model) {}

  async create(gameDto: GameDto): Promise<any> {
    const createdGame = new this.gameModel(gameDto);
    return await createdGame.save();
  }

  async findAll(): Promise<Game[]> {
    return await this.gameModel.find().exec();
  }
  async findById(id): Promise<Game> {
    const customer = await this.gameModel.findById(id).exec();
    return customer;
  }

  async update(id, gameDto: GameDto): Promise<Game> {
    return await this.gameModel.findByIdAndUpdate(id, gameDto, { new: true });
  }
  async delete(id): Promise<Game> {
    return await this.gameModel.findByIdAndRemove(id);
  }
}
