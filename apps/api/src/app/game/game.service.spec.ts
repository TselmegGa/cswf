import { MongooseModule } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { GamesModule } from './game.module';
import { GamesService } from './game.service';
import { GameSchema } from './schemas/game.schema';

describe('GamesService', () => {
  let service: GamesService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot('mongodb+srv://Tselmeg:Tselpass@cluster0.txqktxh.mongodb.net/?retryWrites=true&w=majority',
        { dbName: 'test',useNewUrlParser: true, useUnifiedTopology: true }), // we use Mongoose here, but you can also use TypeORM
        GamesModule,
        MongooseModule.forFeature([{ name: "Game", schema: GameSchema }])
      ], providers: [
        GamesService]
    }).compile();

    service = moduleRef.get<GamesService>(GamesService);
  });


  it('should be true', () => {
    expect(true).toEqual(true);
  });
  
  it('creates a Game', async () => {
    const created = await service.create( {
      name: 'Call of doors',
      genre: 'shooter',
      publisher: "Freeze inc."
    })
    const searched = await service.findById(created._id)
    expect(created.name).toEqual(searched.name)
  })
  it('deletes a Game', async () => {
    const created = await service.create( {
      name: 'Call of doors',
      genre: 'shooter',
      publisher: "Freeze inc."
    })
    await service.delete(created.id)
    const searched = await service.findById(created._id)
    expect(searched).toBeNull()
  })
  it('updates a Game', async () => {
    const created = await service.create( {
      name: 'Call of doors',
      genre: 'shooter',
      publisher: "Freeze inc."
    })
    const updated = await service.update(created.id, {
      name: 'Call of duty',
      genre: 'shooter',
      publisher: "Freaze inc."
    })
    const searched = await service.findById(created._id)
    expect(updated.name).toEqual(searched.name)
  })
  it('gets all Games', async () => {
    const created = await service.create( {
      name: 'Call of doors',
      genre: 'shooter',
      publisher: "Freeze inc."
    })
    const searched = await service.findAll()
    expect(searched.length).toBeGreaterThanOrEqual(1)
  })

});