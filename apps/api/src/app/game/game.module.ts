import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GamesController } from './game.controller';
import { GamesService } from './game.service';
import { Game, GameSchema } from './schemas/game.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: "Game", schema: GameSchema }])],
  controllers: [GamesController],
  providers: [GamesService],
})
export class GamesModule {}
