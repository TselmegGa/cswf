import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type GameDocument = Game & Document;

@Schema()
export class Game {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({type: String, required: true })
  genre: string;

  @Prop({type: String, required: true })
  publisher: string;
}

export const GameSchema = SchemaFactory.createForClass(Game);
GameSchema.post('save', function(doc) {
  console.log('%s has been saved', doc._id);
});

