export class GameDto {
  readonly name: string;
  readonly genre: string;
  readonly publisher: string;
}
