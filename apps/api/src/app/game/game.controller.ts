import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { GamesService } from './game.service';
import { GameDto} from './dto/game.dto';
import { Game } from './schemas/game.schema';

@Controller('games')
export class GamesController {
  constructor(private gamesService: GamesService) {}

  @Post()
  async create(@Body() gameDto: GameDto) {
    return await this.gamesService.create(gameDto);
    //res.status(200).send("Registered.");
  }

  @Get()
  async findAll(){
    return this.gamesService.findAll();
  }
  @Get(':id')
  async findById(@Param('id') id: string){
    return this.gamesService.findById(id);
  }
  @Put(':id')
  async update(@Param('id') id: string, @Body() gameDto: GameDto){
    return this.gamesService.update(id, gameDto);
  }
  @Delete(':id')
  async remove(@Param('id') id: string){
    return this.gamesService.delete(id);
  }

}
