import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { User } from '../user/schemas/user.schema';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: User) {
    const sendUser = await this.usersService.findOne(user.email);
    if(sendUser && sendUser.password === user.password){
      const payload = { username: user.email };
      return {
        user: sendUser,
        access_token: this.jwtService.sign(payload),
      };
    }
    else{
      return UnauthorizedException;

    }

  }
}
