import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './schemas/user.schema';
import { UserDto } from './dto/user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(userDto: UserDto) {
    const createdUser = new this.userModel(userDto);
    return await createdUser.save();
  }

  async findAll(){
    return await this.userModel.find().exec();
  }
  async findOne(email){
    const user = await this.userModel.findOne({email:email}).exec();
    return user;
  }
  async findById(id) {
    const user = await this.userModel.findById(id).exec();
    return user;
  }

  async update(id, userDto: UserDto){
    return await this.userModel.findByIdAndUpdate(id, userDto, { new: true });
  }
  async delete(id) {
    return await this.userModel.findByIdAndRemove(id);
  }
}
