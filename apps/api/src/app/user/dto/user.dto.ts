import {Document} from 'mongoose';

export class UserDto extends Document  {
  readonly email: string;
  readonly fullname: string;
  readonly password: string;
}
