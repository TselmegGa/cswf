import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { UsersService } from './user.service';
import { UserDto } from './dto/user.dto';
import { User } from './schemas/user.schema';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService,) {}

  //@Post()
  //async create(@Body() userDto: UserDto) {
    //return await this.usersService.create(userDto);
    //res.status(200).send("Registered.");
  //}

  @Get()
  async findAll(){
    return this.usersService.findAll();
  }
  @Get(':id')
  async findById(@Param('id') id: string){
    return this.usersService.findById(id);
  }
  //@Put(':id')
  //async update(@Param('id') id, @Body() userDto: UserDto){
    //return this.usersService.update(id, userDto);
  //}
  //@Delete(':id')
  //async remove(@Param('id') id: string){
    //return this.usersService.delete(id);
  //}

}
