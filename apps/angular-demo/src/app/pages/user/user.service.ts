import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { delay, filter, mergeMap, take } from 'rxjs/operators';
import { User } from './user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>('api/users');
  }

  getById(id: string): Observable<User> {
    return this.http.get<User>('api/users/' + id);

  }
  update(user: User): Observable<User> {
    return this.http.put<User>('api/users/' + user._id, user);
  }
  create(user: User): Observable<User> {
    return this.http.post<User>('api/users', user);
  }
}
