import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from '../user.model';
import { UserService } from '../user.service';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../../../shared/auth.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
})
export class UserDetailComponent implements OnInit, OnDestroy {
  user$: Observable<User>
  sub?:Subscription
  constructor(
    private userService: UserService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    
    this.sub = this.authService.getUserFromLocalStorage().subscribe((data: User) =>
    {
      this.user$ =this.userService.getById(data._id)
    })

  }
  ngOnDestroy(): void {
    this.sub?.unsubscribe()
  }
}
