import { Entity } from '../../shared/common/entity';

export class User extends Entity {
  email: string
  fullname: string
  password: string
  
  token: string
}
