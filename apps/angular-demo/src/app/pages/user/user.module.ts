import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from '.';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from '../../shared/authguard/auth.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: fromComponents.UserDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'update',
    pathMatch: 'full',
    component: fromComponents.UserEditComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  declarations: [...fromComponents.components],
  imports: [CommonModule, RouterModule.forChild(routes), HttpClientModule,FormsModule, ReactiveFormsModule],
})
export class UserModule {}
