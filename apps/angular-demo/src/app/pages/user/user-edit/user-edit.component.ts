import { Component, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute, ParamMap, Router } from '@angular/router'
import { User } from '../user.model'
import { UserService } from '../user.service'
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap'
import { tap, map, switchMap, catchError, takeUntil } from 'rxjs/operators'
import { Observable, of, Subject, Subscription } from 'rxjs'
import { Param } from '@nestjs/common'
import { AuthService } from '../../../shared/auth.service'
import { AlertService } from '../../../shared/alert/alert.service'

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit,OnDestroy {
  user: User;
  title: string;
  ngDestroy$ = new Subject();
  constructor(private userService: UserService,
    private authService: AuthService, private route: ActivatedRoute, private router: Router, private alertService: AlertService) {}

  ngOnInit() {
    this.authService.getUserFromLocalStorage().pipe(takeUntil(this.ngDestroy$)).subscribe((data: User) =>
    {
      this.userService.getById(data._id).pipe(takeUntil(this.ngDestroy$)).subscribe((data: User) =>{
        this.user = data
      })
    })
  }

  onSubmit() {
    console.log('onSubmit: ', this.user)

      this.userService.update(this.user).pipe(takeUntil(this.ngDestroy$)).subscribe({
        next: () => {
            this.alertService.success('User updated');
        },
        error: error => {
            this.alertService.error(error);
        }
    })
  }
  ngOnDestroy(): void {
    this.ngDestroy$.next(true)
    this.ngDestroy$.complete()
  }
}
