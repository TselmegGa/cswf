import { UserDetailComponent } from './user-detail/user-detail.component';

import { UserEditComponent } from './user-edit/user-edit.component';

export const components: any[] = [
  UserDetailComponent,
  UserEditComponent,
];

export * from './user-detail/user-detail.component';
export * from './user-edit/user-edit.component';
