import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';

  useCases: UseCase[] = [
    {
      id: 'UC-LO',
      name: 'Inloggen',
      description: 'Gebruiker gaat inloggen.',
      scenario: [
        'De gebruiker drukt op de login knop.',
        'De gebruiker vult in de informatie .',
        'De gebruiker drukt op de log in knop.',
      ],
      actor: this.ADMIN_USER,
      precondition: 'De gebruiker is heeft de website open.',
      postcondition: 'De gebruiker is ingelogd.',
    },
    {
      id: 'UC-RE',
      name: 'Registeren',
      description: 'Gebruiker gaat registeren.',
      scenario: [
        'De gebruiker drukt op de register knop.',
        'De gebruiker vult in de informatie .',
        'De gebruiker drukt op de register knop.',
      ],
      actor: this.ADMIN_USER,
      precondition: 'De gebruiker is heeft de website open.',
      postcondition: 'De gebruiker is geregisterd.',
    },
    {
      id: 'UC-01',
      name: 'Player lijst bekijken',
      description: 'Player details bekijken vanaf de dashboard.',
      scenario: [
        'De gebruiker drukt op de player knop.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'De gebruiker is heeft de website open.',
      postcondition: 'De player lijst is getoond.',
    },
    {
      id: 'UC-02',
      name: 'Player details bekijken.',
      description: 'Player details bekijken vanaf de player lijst.',
      scenario: [
        'De gebruiker drukt op de naam van de player.',
    ]
      ,
      actor: this.PLAIN_USER,
      precondition: 'De player lijst is getoond.',
      postcondition: 'De player details is getoond.',
    },
    {
      id: 'UC-03',
      name: 'Player details verwijzigen.',
      description: 'Player details verwijzigen vanaf de player lijst.',
      scenario: [
        'De gebruiker drukt op de edit knop van de player.',
        'De gebruiker vult in de informatie van de player.',
        'De gebruiker drukt op de save knop.',
    ]
      ,
      actor: this.ADMIN_USER,
      precondition: 'De player lijst is getoond en de gebruiker is ingelogd.',
      postcondition: 'De player details is gewijzigd.',
    },
    {
      id: 'UC-04',
      name: 'Player details verwijderen.',
      description: 'Player details verwijderen vanaf de player lijst.',
      scenario: [
        'De gebruiker drukt op de delete knop van de player.',
    ]
      ,
      actor: this.ADMIN_USER,
      precondition: 'De player lijst is getoond en de gebruiker is ingelogd.',
      postcondition: 'De player details is verwijderd.',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
