import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, of, Subject, Subscription } from 'rxjs';
import { Competition } from '../competition.model';
import { CompetitionService } from '../competition.service';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { AlertService } from '../../../shared/alert';


@Component({
  selector: 'app-competition-list',
  templateUrl: './competition-list.component.html',
  styleUrls: ['./competition-list.component.css']
})
export class CompetitionListComponent implements OnInit, OnDestroy {
  competition$: Observable<Competition[]>;
 
  err: string;
  ngDestroy$ = new Subject();

  constructor(private competitionService: CompetitionService, private route: ActivatedRoute, private router: Router ,private alertService: AlertService) { }

  ngOnInit(): void {
    console.log('Competition list loaded!');
    this.competitionService.getAll().pipe(takeUntil(this.ngDestroy$)).subscribe((data: Competition[])=>{
      this.competition$ = of(data);
    },(err: string) => {
      this.alertService.error(err);
    })
  }
  delete(id){
    this.competitionService.delete(id).pipe(takeUntil(this.ngDestroy$)).subscribe({
      next: data => {
        this.router.navigate([''], { relativeTo: this.route })   
      },
      error: error => {
          console.error('There was an error!', error);
      }
  });
  }
  ngOnDestroy(){
    this.ngDestroy$.next(true);
    this.ngDestroy$.complete();
  }
}
