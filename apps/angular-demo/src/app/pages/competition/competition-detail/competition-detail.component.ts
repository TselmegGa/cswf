import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { delay, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { Competition } from '../competition.model';
import { CompetitionService } from '../competition.service';
import { AlertService } from '../../../shared/alert';

@Component({
  selector: 'app-competition-detail',
  templateUrl: './competition-detail.component.html',
  styleUrls: ['./competition-detail.component.css']
})
export class CompetitionDetailComponent implements OnInit, OnDestroy {
  competition: Competition;
  ngDestroy$ = new Subject();
  constructor(private competitionService: CompetitionService, private route: ActivatedRoute,private alertService: AlertService) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(takeUntil(this.ngDestroy$)).subscribe((params: ParamMap)=>{
    this.competitionService.getById(params.get('id')).pipe(takeUntil(this.ngDestroy$)).subscribe((data: Competition)=>{
      this.competition = data;
    }),(err: string) => {
      this.alertService.error(err);
    }
  
  })
}
  ngOnDestroy(){
    this.ngDestroy$.next(true);
    this.ngDestroy$.complete();
  }
  

}