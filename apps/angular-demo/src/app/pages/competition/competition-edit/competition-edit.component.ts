import { Component, OnDestroy, OnInit } from '@angular/core';
import { Competition } from '../competition.model';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { CompetitionService } from '../competition.service';
import { Game } from '../../game/game.model';
import { Player } from '../../player/player.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AlertService } from '../../../shared/alert/alert.service';
import { PlayerService } from '../../player/player.service';
import { GameService } from '../../game/game.service';

@Component({
  selector: 'app-competition-edit',
  templateUrl: './competition-edit.component.html',
  styleUrls: ['./competition-edit.component.css']
})
export class CompetitionEditComponent implements OnInit, OnDestroy {

  competition: Competition;
  title: string;
  games: Game[] = [];
  players: Player[] = [];
  id: string | null = null;
  ngDestroy$ = new Subject();

  constructor(private competitionService: CompetitionService,private playerService: PlayerService,private gameService: GameService, private route: ActivatedRoute, private router: Router, private alertService: AlertService) {}

  ngOnInit() {
    this.gameService.getAll().pipe(takeUntil(this.ngDestroy$)).subscribe((data: Game[])=>{
      this.games = data;
    },(err: string) => { 
      this.alertService.error(err);
    })
    this.playerService.getAll().pipe(takeUntil(this.ngDestroy$)).subscribe((data: Player[])=>{
      this.players = data;
    },(err: string) => {
      this.alertService.error(err);
    })
    
    this.route.paramMap.pipe(takeUntil(this.ngDestroy$)).subscribe((params: ParamMap)=>{
     
          this.id = params.get('id');
          
          if(this.id){
            this.title = "Edit Competition";
            this.competitionService.getById(this.id).pipe(takeUntil(this.ngDestroy$)).subscribe((data: Competition)=>{
              this.competition = data;
            },(err: string) => {
              this.alertService.error(err);
            }
            )  
            
          }else{
            this.title = "New Competition";
            this.competition = new Competition(undefined)
          }
        })
       
  }
  onSubmit() {
    if(!this.competition.players.some(player => player._id === this.competition.winner._id)){
      this.competition.players.push(this.competition.winner)
    }
        if(this.competition._id){
        this.competitionService.update(this.competition).pipe(takeUntil(this.ngDestroy$)).subscribe({
          next: () => {
              this.alertService.success('Competition updated');

          },
          error: error => {
              this.alertService.error(error);
          }
      })
        
      }else{
        this.competitionService.create(this.competition).pipe(takeUntil(this.ngDestroy$)).subscribe({
          next: () => {
              this.alertService.success('Competition added');
          },
          error: error => {
              this.alertService.error(error);
          }
      })
        
      }
    
  }

  ngOnDestroy(){
    this.ngDestroy$.next(true);
    this.ngDestroy$.complete();
  }


}
