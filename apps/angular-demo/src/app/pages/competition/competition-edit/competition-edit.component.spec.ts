import { Component, Directive, HostListener, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { BehaviorSubject, of, throwError } from 'rxjs';
import { AlertService } from '../../../shared/alert';
import { AuthService } from '../../../shared/auth.service';
import { Game } from '../../game/game.model';
import { GameService } from '../../game/game.service';
import { Player } from '../../player/player.model';
import { PlayerService } from '../../player/player.service';
import { User } from '../../user/user.model';
import { Competition } from '../competition.model';
import { CompetitionService } from '../competition.service';

import { CompetitionEditComponent } from './competition-edit.component';


@Component({ selector: 'app-spinner', template: '' })
class AppSpinnerStubComponent {}

@Directive({
  selector: '[routerLink]',
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onClick(): void {
    this.navigatedTo = this.linkParams;
  }
}

// Global mock objects
const expectedUserData: User = {
  _id: 'mongo_id',
  fullname: 'Tselmeg Gantuya',
  email: 'user@host.com',
  password: 're@lpassword',
  token: 'some.dummy.token',
};
const games: Game[] = [{ _id: 'game1', name: 'Red Alert 4', genre: 'RTS', publisher: 'EA' },
{ _id: 'game2', name: 'Red Alert 3', genre: 'RTS', publisher: 'EA' }]
const players: Player[] = [{
  _id: 'player1',
  name: 'Stabby',
  birthday:  new Date('1995-12-17T03:24:00'),
  bio: 'Anatoliy "Stabby" Ranjeet is a professional RTS player. He plays Red alert 4, Red alert 3 and They are billions.'
},{
  _id: 'player12',
  name: 'Sneaky',
  birthday:  new Date('1998-11-17T03:24:00'),
  bio: 'Mark "Sneaky" Johnson is a professional RTS player. He plays Red alert 4 and Red alert 3 .'
},{
  _id: 'player13',
  name: 'Cope',
  birthday:  new Date('1997-12-16T03:24:00'),
  bio: 'Muirchertach "Cope" Raghu is a professional RTS player. He plays Red alert 4, Red alert 3 and They are billions.'
},{
  _id: 'player14',
  name: 'Lard',
  birthday:  new Date('1996-10-17T03:24:00'),
  bio: 'Ælfgifu "Lard" Aylmer is a professional RTS player. He plays Red alert 4 and They are billions.'
},]
const expectedCompetition: Competition[] = [
  {
    _id: 'mongo_id',
    name: "RTSunday 2020",
    game: { _id: 'game1', name: 'Red Alert 4', genre: 'RTS', publisher: 'EA' },
    winner: {
      _id: 'player1',
      name: 'Stabby',
      birthday:  new Date('1995-12-17T03:24:00'),
      bio: 'Mark "Stabby" Johnson is a professional RTS player. He plays Red alert 4, Red alert 3 and They are billions.'
    },
    description: "RTSunday 2020 was a competition organized on January 12 2020. This competition had a grandprice of $30,000.",
    players: [{
      _id: 'player1',
      name: 'Stabby',
      birthday:  new Date('1995-12-17T03:24:00'),
      bio: 'Anatoliy "Stabby" Ranjeet is a professional RTS player. He plays Red alert 4, Red alert 3 and They are billions.'
    },{
      _id: 'player12',
      name: 'Sneaky',
      birthday:  new Date('1998-11-17T03:24:00'),
      bio: 'Mark "Sneaky" Johnson is a professional RTS player. He plays Red alert 4 and Red alert 3 .'
    },]
  },
];

describe('CompetitionEditComponent', () => {
  let component: CompetitionEditComponent;
  let fixture: ComponentFixture<CompetitionEditComponent>;

  let alertServiceSpy;
  let competitionServiceSpy;
  let authServiceSpy;
  let gameServiceSpy;
  let playerServiceSpy;
  let routerSpy;
  let compiled;

  beforeEach(async () => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', [
      'error',
      'success',
    ]);
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'register',
      'logout',
      'getUserFromLocalStorage',
      'saveUserToLocalStorage',
      'userMayEdit',
    ]);
    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authServiceSpy.currentUser$ = mockUser$;

    competitionServiceSpy = jasmine.createSpyObj('CompetitionService', ['getAll', 'update', 'create', 'getById', 'getByGame', 'delete']);
    gameServiceSpy = jasmine.createSpyObj('GameService', ['getAll']);
    playerServiceSpy = jasmine.createSpyObj('PlayerService', ['getAll']);
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

    TestBed.configureTestingModule({
      // The declared components needed to test the UsersComponent.
      declarations: [
        CompetitionEditComponent, 
        RouterLinkStubDirective, 
        AppSpinnerStubComponent,
      ],
      imports: [FormsModule],
      //
      // The constructor of our real component uses dependency injected services
      // Never provide the real service in testcases!
      //
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: CompetitionService, useValue: competitionServiceSpy },
        { provide: PlayerService, useValue: playerServiceSpy },
        { provide: GameService, useValue: gameServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: '619bdb5e3b174a700c923da3',
              })
            ),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CompetitionEditComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  fit('should create', (done) => {
    competitionServiceSpy.getById.and.returnValue(of(expectedCompetition[0]));
    playerServiceSpy.getAll.and.returnValue(of(players));
    gameServiceSpy.getAll.and.returnValue(of(games));

    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.competition).toEqual(expectedCompetition[0]);
    expect(component.games).toEqual(games);
    expect(component.players).toEqual(players);
    done();
  });
  fit('should receive error when users service fails onInit()', () => {
    const expectedError = 'service error occurred';
    competitionServiceSpy.getById.and.returnValue(of(expectedCompetition[0]));
    playerServiceSpy.getAll.and.returnValue(of(players));
    gameServiceSpy.getAll.and.returnValue(throwError(expectedError));

    fixture.detectChanges();  // or component.ngOnInit();
    expect(component.games.length).toBeLessThan(1);
    expect(component.games).not.toEqual(games); // for primitives
  });
  
  it('should correctly render the passed Input value', () => {
    competitionServiceSpy.getById.and.returnValue(of(expectedCompetition[0]));
    playerServiceSpy.getAll.and.returnValue(of(players));
    gameServiceSpy.getAll.and.returnValue(of(games));

    fixture.detectChanges(); 

  
    expect(component.competition.name).toBe(compiled.querySelector('#mainForm').querySelectorAll('input')[0].innerHTML); // 3
  });
});
