import { CompetitionEditComponent } from './competition-edit/competition-edit.component';
import { CompetitionDetailComponent } from './competition-detail/competition-detail.component';
import { CompetitionListComponent } from './competition-list/competition-list.component';
import { CompetitionGameComponent } from './competition-game/competition-game.component';

export const components: any[] = [
  CompetitionDetailComponent,
  CompetitionEditComponent,
  CompetitionListComponent,
  CompetitionGameComponent
];

export * from './competition-detail/competition-detail.component';
export * from './competition-list/competition-list.component';
export * from './competition-edit/competition-edit.component';
export * from './competition-game/competition-game.component';
