import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { delay, switchMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Competition } from '../competition.model';
import { CompetitionService } from '../competition.service';

@Component({
  selector: 'app-competition-game',
  templateUrl: './competition-game.component.html',
  styleUrls: ['./competition-game.component.css']
})
export class CompetitionGameComponent implements OnInit {
  competition$: Observable<Competition[]>;
  constructor(private competitionService: CompetitionService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.competition$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.competitionService.getByGame(params.get('id'))
      )
    );
  }
}
