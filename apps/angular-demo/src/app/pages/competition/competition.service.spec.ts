import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Observable, of, throwError } from 'rxjs';
import { User } from '../user/user.model';
import { Competition } from './competition.model';

import { CompetitionService } from './competition.service';

const expectedUserData: User = {
  _id: 'mongo_id',
  fullname: 'Tselmeg Gantuya',
  email: 'user@host.com',
  password: 're@lpassword',
  token: 'some.dummy.token',
};
const expectedCompetition: Competition[] = [
  {
    _id: 'mongo_id',
    name: "RTSunday 2020",
    game: { _id: 'mongo_id2', name: 'Red Alert 4', genre: 'RTS', publisher: 'EA' },
    winner: {
      _id: 'mongo_id3',
      name: 'Stabby',
      birthday:  new Date('1995-12-17T03:24:00'),
      bio: 'Mark "Stabby" Johnson is a professional RTS player. He plays Red alert 4, Red alert 3 and They are billions.'
    },
    description: "RTSunday 2020 was a competition organized on January 12 2020. This competition had a grandprice of $30,000.",
    players: [{
      _id: 'mongo_id3',
      name: 'Stabby',
      birthday:  new Date('1995-12-17T03:24:00'),
      bio: 'Anatoliy "Stabby" Ranjeet is a professional RTS player. He plays Red alert 4, Red alert 3 and They are billions.'
    },{
      _id: 'mongo_id4',
      name: 'Sneaky',
      birthday:  new Date('1998-11-17T03:24:00'),
      bio: 'Mark "Sneaky" Johnson is a professional RTS player. He plays Red alert 4 and Red alert 3 .'
    },{
      _id: 'mongo_id5',
      name: 'Cope',
      birthday:  new Date('1997-12-16T03:24:00'),
      bio: 'Muirchertach "Cope" Raghu is a professional RTS player. He plays Red alert 4, Red alert 3 and They are billions.'
    },{
      _id: 'mongo_id6',
      name: 'Lard',
      birthday:  new Date('1996-10-17T03:24:00'),
      bio: 'Ælfgifu "Lard" Aylmer is a professional RTS player. He plays Red alert 4 and They are billions.'
    },]
  },
];
describe('CompetitionService', () => {
  let httpSpy: jasmine.SpyObj<HttpClient>;
  let service: CompetitionService;

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete', 'put']);
    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: httpSpy }],
    });
    service = TestBed.inject(CompetitionService);
    httpSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  fit('should be created', (done: DoneFn) => {
    httpSpy.post.and.returnValue(of(expectedCompetition[0]));

    service.create(expectedCompetition[0]).subscribe((competition: Competition) => {
      console.log(competition);
      expect(competition._id).withContext('expected competitions').toEqual(expectedCompetition[0]._id);
      done();
    });
  });

  fit('should be updated', (done: DoneFn) => {
    httpSpy.put.and.returnValue(of(expectedCompetition[0]));

    service.update(expectedCompetition[0]).subscribe((competition: Competition) => {
      console.log(competition);
      expect(competition._id).withContext('expected competitions').toEqual(expectedCompetition[0]._id);
      done();
    });
  });
  fit('should be updated', (done: DoneFn) => {
    httpSpy.delete.and.returnValue(of(expectedCompetition[0]));

    service.delete(expectedCompetition[0]._id).subscribe((competition: Competition) => {
      console.log(competition);
      expect(competition._id).withContext('expected competitions').toEqual(expectedCompetition[0]._id);
      done();
    });
  });

  fit('should return a list of competitions', (done: DoneFn) => {
    httpSpy.get.and.returnValue(of(expectedCompetition));

    service.getAll().subscribe((competitions: Competition[]) => {
      console.log(competitions);
      expect(competitions.length).withContext('one call').toBe(1);
      expect(competitions[0]._id).withContext('expected competitions').toEqual(expectedCompetition[0]._id);
      done();
    });
  });
  
  fit('should make a GET HTTP request with id appended to end of url', (done: DoneFn) => {
    httpSpy.get.and.returnValue(of(expectedCompetition[0]));

    service.getById("mongo_id").subscribe((res: Competition) => {
      console.log(res);
      expect(res._id).withContext('expected competition').toEqual(expectedCompetition[0]._id);
      done();
     }); 
   });

  fit('should return an error when the server returns a 404', (done: DoneFn) => {
  
    httpSpy.get.and.returnValue(throwError({
      error: 'test 404 error',
      status: 404, statusText: 'Not Found'
    }));
  
    service.getAll().subscribe(
      data => fail('Should have failed with 404 error'),
      (error: HttpErrorResponse) => {
        expect(error.status).toEqual(404);
        expect(error.error).toContain('404 error');
        done();
      });
      
    });

});
