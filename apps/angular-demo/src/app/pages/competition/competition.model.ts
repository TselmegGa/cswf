import { Entity } from '../../shared/common/entity';
import { Game } from '../game/game.model';
import { Player } from '../player/player.model';

export class Competition extends Entity {
  name: string;
  game: Game;
  winner: Player;
  description: string;
  players: Player[];
}
