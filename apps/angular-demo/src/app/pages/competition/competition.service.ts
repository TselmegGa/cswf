import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Game } from '../game/game.model';
import { Player } from '../player/player.model';
import { Competition } from './competition.model';


@Injectable({
  providedIn: 'root'
})
export class CompetitionService {

    constructor(private http: HttpClient) { }

    getAll(): Observable<Competition[]> {
      return this.http.get<Competition[]>('api/competitions');

    }
    getById(id: string): Observable<Competition> {
      return this.http.get<Competition>('api/competitions/' + id);
    }
    getByGame(id: string): Observable<Competition[]> {
      return this.http.get<Competition[]>('api/competitions/game/' + id);
    }
    update(obj: Competition): Observable<Competition> {
      return this.http.put<Competition>('api/competitions/' + obj._id, obj,{ headers: { 'Content-Type': 'application/json' }}).pipe(
        catchError(err => {
          throw 'error in source. Details: ' + err;
        })
      );
    }
    
    create(obj: Competition): Observable<Competition> {
      return this.http.post<Competition>('api/competitions/', obj,{ headers: { 'Content-Type': 'application/json' }});
    }
    delete(id: string): Observable<Competition> {
      return this.http.delete<Competition>('api/competitions/' + id);
    }


}
