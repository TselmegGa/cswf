import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from '.';
import { AuthGuard } from '../../shared/authguard/auth.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';



const routes: Routes = [
  { path: '', pathMatch: 'full', component: fromComponents.CompetitionListComponent, canActivate: [AuthGuard] },
  {
    path: ':id',
    pathMatch: 'full',
    component: fromComponents.CompetitionDetailComponent,
    
  },
  {
    path: 'game/:id',
    pathMatch: 'full',
    component: fromComponents.CompetitionGameComponent,
  },
  {
    path: 'create/competition',
    pathMatch: 'full',
    component: fromComponents.CompetitionEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'update/:id',
    pathMatch: 'full',
    component: fromComponents.CompetitionEditComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  declarations: [...fromComponents.components],
  imports: [CommonModule, RouterModule.forChild(routes), HttpClientModule,FormsModule, ReactiveFormsModule]
})
export class CompetitionsModule { }
