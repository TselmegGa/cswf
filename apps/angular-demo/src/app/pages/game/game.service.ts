import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Game } from './game.model';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Game[]>{
    return this.http.get<Game[]>('api/games');

  }
  getById(id: string): Observable<Game> {
    return this.http.get<Game>('api/games/' + id);
  }
  update(obj: Game): Observable<Game> {
    return this.http.put<Game>('api/games/' + obj._id, obj,{ headers: { 'Content-Type': 'application/json' }}).pipe(
      catchError(err => {
        throw 'error in source. Details: ' + err;
      })
    );
  }
  
  create(obj: Game): Observable<Game> {
    return this.http.post<Game>('api/games/', obj,{ headers: { 'Content-Type': 'application/json' }});
  }
  delete(id: string): Observable<Game> {
    return this.http.delete<Game>('api/games/' + id);
  }
}
