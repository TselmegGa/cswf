import { Entity } from '../../shared/common/entity';

export class Game extends Entity {
  name: string;
  genre: string;
  publisher: string;
}
