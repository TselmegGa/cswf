import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Game } from '../game.model';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit, OnDestroy {
  game$: Observable<Game[]>;
  sub?: Subscription
  constructor(private gameService: GameService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    console.log('Game list loaded!');
    this.game$ = this.gameService.getAll();
  }
  delete(id){
    this.sub = this.gameService.delete(id).subscribe({
      next: data => {
        this.router.navigate([''], { relativeTo: this.route })   
      },
      error: error => {
          console.error('There was an error!', error);
      }
  });
  }
  ngOnDestroy(): void {
    this.sub?.unsubscribe()
  }
}
