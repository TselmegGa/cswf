import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AlertService } from '../../../shared/alert/alert.service';
import { Game } from '../game.model';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game-edit',
  templateUrl: './game-edit.component.html',
  styleUrls: ['./game-edit.component.css']
})
export class GameEditComponent implements OnInit, OnDestroy {

  game: Game;
  title: string;
  ngDestroy$ = new Subject();
  id: string | null = null;

  constructor(private gameService: GameService, private route: ActivatedRoute, private router: Router, private alertService: AlertService) {}

  ngOnInit() {

    this.route.paramMap.pipe(takeUntil(this.ngDestroy$)).subscribe((params: ParamMap)=>{
          this.id = params.get('id');
          
          if(this.id){
            this.title = "Edit Game";
            this.gameService.getById(this.id).pipe(takeUntil(this.ngDestroy$)).subscribe((data: Game)=>{
              this.game = data;
            })  
            
          }else{
            this.title = "New Game";
            this.game = new Game(undefined)
          }
        })

  }
  onSubmit() {
    console.log('onSubmit: ', this.game)

      if(this.game._id){
        this.gameService.update(this.game).pipe(takeUntil(this.ngDestroy$)).subscribe({
          next: () => {
              this.alertService.success('Game updated');
          },
          error: error => {
              this.alertService.error(error);
          }
      })
        
      }else{
        this.gameService.create(this.game).pipe(takeUntil(this.ngDestroy$)).subscribe({
          next: () => {
              this.alertService.success('Game added');
          },
          error: error => {
              this.alertService.error(error);
          }
      })
        
      }
    
  }
  ngOnDestroy(){
    this.ngDestroy$.next(true);
    this.ngDestroy$.complete();
  }

}
