import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from '.';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from '../../shared/authguard/auth.guard';


const routes: Routes = [
  { path: '', pathMatch: 'full', component: fromComponents.GameListComponent },
  {
    path: ':id',
    pathMatch: 'full',
    component: fromComponents.GameDetailComponent,
  },
  {
    path: 'create/game',
    pathMatch: 'full',
    component: fromComponents.GameEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'update/:id',
    pathMatch: 'full',
    component: fromComponents.GameEditComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  declarations: [...fromComponents.components],
  imports: [CommonModule, RouterModule.forChild(routes), HttpClientModule,FormsModule, ReactiveFormsModule]
})
export class GameModule { }
