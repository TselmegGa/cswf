import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { delay, switchMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Game } from '../game.model';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})
export class GameDetailComponent implements OnInit {
  game$: Observable<Game>;
  constructor(private gameService: GameService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.game$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.gameService.getById(params.get('id'))
      )
    );
  }

}
