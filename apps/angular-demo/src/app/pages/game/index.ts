import { GameEditComponent } from './game-edit/game-edit.component';
import { GameDetailComponent } from './game-detail/game-detail.component';
import { GameListComponent } from './game-list/game-list.component';

export const components: any[] = [
  GameDetailComponent,
  GameEditComponent,
  GameListComponent,
];

export * from './game-detail/game-detail.component';
export * from './game-list/game-list.component';
export * from './game-edit/game-edit.component';
