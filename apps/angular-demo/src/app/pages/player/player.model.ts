import { Entity } from '../../shared/common/entity';

export class Player extends Entity {
  name: string;
  birthday: Date;
  bio: string;
}
