import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { delay, switchMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Player } from '../player.model';
import { PlayerService } from '../player.service';

@Component({
  selector: 'app-player-detail',
  templateUrl: './player-detail.component.html',
  styleUrls: ['./player-detail.component.css']
})
export class PlayerDetailComponent implements OnInit {
  player$: Observable<Player>;

  constructor(private playerService: PlayerService, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.player$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.playerService.getById(params.get('id'))
      )
    );
  }
  calculateAge(date) 
  {
    let timeDiff = Math.abs(Date.now() - Date.parse(date));
    let age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);

    return age

  }

}
