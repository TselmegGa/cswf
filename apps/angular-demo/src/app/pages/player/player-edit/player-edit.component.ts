import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AlertService } from '../../../shared/alert/alert.service';
import { Player } from '../player.model';
import { PlayerService } from '../player.service';

@Component({
  selector: 'app-player-edit',
  templateUrl: './player-edit.component.html',
  styleUrls: ['./player-edit.component.css']
})
export class PlayerEditComponent implements OnInit, OnDestroy {
  player: Player;
  title: string;
  playerDate:string;
  id: string | null = null;
  ngDestroy$ = new Subject();

  constructor(private playerService: PlayerService, private route: ActivatedRoute, private router: Router, private alertService: AlertService) {}

  ngOnInit() {
    this.route.paramMap.pipe(takeUntil(this.ngDestroy$)).subscribe((params: ParamMap)=>{
          this.id = params.get('id');
          
          if(this.id){
            this.title = "Edit Player";
            this.playerService.getById(this.id).pipe(takeUntil(this.ngDestroy$)).subscribe((data: Player)=>{
              this.player = data;
              this.playerDate = data.birthday.toString()
              console.log(this.player);
            })  
            
          }else{
            this.title = "New Player";
            this.player = new Player(undefined)
          }
        })

  }

  onSubmit() {
    console.log('onSubmit: ', this.player)

      console.log(this.player)
      this.player.birthday = new Date(this.playerDate)
      if(this.player._id){
        this.playerService.update(this.player).pipe(takeUntil(this.ngDestroy$)).subscribe({
          next: () => {
              this.alertService.success('Player updated');
          },
          error: error => {
              this.alertService.error(error);
          }
      })
        
      }else{
        this.playerService.create(this.player).pipe(takeUntil(this.ngDestroy$)).subscribe({
          next: () => {
              this.alertService.success('Player added');

          },
          error: error => {
              this.alertService.error(error);
          }
      })
        
      }
    
  }
  ngOnDestroy(){
    this.ngDestroy$.next(true);
    this.ngDestroy$.complete();
  }

}
