import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Player } from '../player.model';
import { PlayerService } from '../player.service';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit, OnDestroy {

  player$?: Observable<Player[]>;
  sub?: Subscription
  constructor(private playerService: PlayerService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    console.log('Player list loaded!');
    this.player$ = this.playerService.getAll();
  }
  calculateAge(date) 
  {
    let timeDiff = Math.abs(Date.now() - Date.parse(date));
    let age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);

    return age

  }
  delete(id){
    this.sub = this.playerService.delete(id).subscribe({
      next: data => {
        this.router.navigate([''], { relativeTo: this.route })   
      },
      error: error => {
          console.error('There was an error!', error);
      }
  });
  }
  ngOnDestroy(): void {
    this.sub?.unsubscribe()
  }
}
