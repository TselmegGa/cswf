import { PlayerListComponent } from './player-list/player-list.component';
import { PlayerDetailComponent } from './player-detail/player-detail.component';
import { PlayerEditComponent } from './player-edit/player-edit.component';

export const components: any[] = [
  PlayerDetailComponent,
  PlayerEditComponent,
  PlayerListComponent,
];

export * from './player-detail/player-detail.component';
export * from './player-list/player-list.component';
export * from './player-edit/player-edit.component';
