import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Player } from './player.model';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Player[]>{
    return this.http.get<Player[]>('api/players');

  }
  getById(id: string): Observable<Player> {
    return this.http.get<Player>('api/players/' + id);
  }
  update(player: Player): Observable<Player> {
    return this.http.put<Player>('api/players/' + player._id, player,{ headers: { 'Content-Type': 'application/json' }}).pipe(
      catchError(err => {
        throw 'error in source. Details: ' + err;
      })
    );
  }
  
  create(player: Player): Observable<Player> {
    return this.http.post<Player>('api/players/', player,{ headers: { 'Content-Type': 'application/json' }});
  }
  delete(id: string): Observable<Player> {
    return this.http.delete<Player>('api/players/' + id);
  }
}
