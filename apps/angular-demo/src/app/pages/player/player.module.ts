import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import * as fromComponents from '.';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from '../../shared/authguard/auth.guard';


const routes: Routes = [
  { path: '', pathMatch: 'full', component: fromComponents.PlayerListComponent },
  {
    path: ':id',
    pathMatch: 'full',
    component: fromComponents.PlayerDetailComponent,
  },
  {
    path: 'create/player',
    pathMatch: 'full',
    component: fromComponents.PlayerEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'update/:id',
    pathMatch: 'full',
    component: fromComponents.PlayerEditComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  declarations: [...fromComponents.components],
  imports: [CommonModule, RouterModule.forChild(routes), HttpClientModule,FormsModule, ReactiveFormsModule]
})
export class PlayerModule { }
