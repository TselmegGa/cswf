import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { User } from '../../pages/user/user.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: User;
  submitted = false;
  constructor(private http: HttpClient,private router: Router) { }


  ngOnInit(): void {
    this.user = new User('');
  }


  onSubmit(): void {
    this.submitted = true;

    if (this.user.email && this.user.password && this.user.fullname) {
      this.http.post<User>('api/users', this.user,{ headers: { 'Content-Type': 'application/json' }})
      .subscribe((user: User)=>{
          this.router.navigateByUrl('/login');
        }) 
    } else {
      console.error('Email, name or password is invalid');
    }
  }

}
