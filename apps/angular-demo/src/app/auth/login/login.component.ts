import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../pages/user/user.model';
import { AuthService } from '../../shared/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  user: User;
  submitted = false;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.user = new User('');
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.user.email && this.user.password) {
      this.authService.login(this.user).subscribe((data) => {
        console.log(data);
        this.router.navigateByUrl('/dashboard');
      });
    } else {
      console.error('Email or password is invalid');
    }
  }
}
