import { ComponentFixture, TestBed } from '@angular/core/testing';
import { User } from '../../pages/user/user.model';

import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
describe('Login and log off', () => {
  let component: NavbarComponent;
let fixture: ComponentFixture<NavbarComponent>;
let login: HTMLElement;
let logout: HTMLElement;

beforeEach(() => {
  TestBed.configureTestingModule({
    declarations: [ NavbarComponent ],
  });
  fixture = TestBed.createComponent(NavbarComponent);
  component = fixture.componentInstance; // BannerComponent test instance
  logout = fixture.nativeElement.querySelector('#logout');
  login = fixture.nativeElement.querySelector('#login');
});
  it('login button should exist', () => {
    expect(login.textContent).toContain("login")
  });

  it('logout button should exist if has user', () => {
    component.user = new User('');
    component.user.token = "real token";
    expect(login.textContent).toContain("logout")
  });
});
