import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../../pages/user/user.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit, OnDestroy {
  @Input() title: string;
  user: User;
  sub?:Subscription;
  isNavbarCollapsed = true;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.title = "GOG"
    this.sub = this.authService.getUserFromLocalStorage().subscribe((data) =>
    {
      this.user = data
    })
  }
  ngOnDestroy(): void {
    this.sub?.unsubscribe()
  }

  onLogout(){
    this.authService.logout()
    this.user = new User('')
  }

}
