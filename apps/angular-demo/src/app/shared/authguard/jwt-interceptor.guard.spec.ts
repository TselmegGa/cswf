import { TestBed } from '@angular/core/testing';

import { JwtInterceptor } from './jwt-interceptor';

xdescribe('JwtInterceptorGuard', () => {
  let guard: JwtInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(JwtInterceptor);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
