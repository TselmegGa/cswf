import { Entity } from './entity';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { EMPTY, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AlertService } from '../alert/alert.service';

export abstract class EntityService<T extends Entity> {
  constructor(
    public readonly http: HttpClient,
    public readonly url: string,
    public readonly endpoint: string,
    public alertService = new AlertService(),
    public object = localStorage.getItem('currentuser'),
    public headerOptions = new HttpHeaders({
      'Authorization': `Bearer ${JSON.parse(object).entity.token}`})
  ) { }



  public list(params?: HttpParams): Observable<T[]>
  {
    //const object = localStorage.getItem('currentuser');
    const endpoint = `${this.url}${this.endpoint}`

    console.log(`list ${endpoint} (params = ${params})`);

    return this.http
        .get<T[]>(endpoint, { headers: this.headerOptions, params: params}).pipe(
        catchError( error => {
        console.log(error);
        this.alertService.error(error.error.message || error.message);
        return EMPTY;
      })
    );
  }

  public read(params?: HttpParams, id?: string): Observable<T | undefined>
  {
    const endpoint = `${this.url}${this.endpoint}/${id}`


    console.log(`read ${endpoint} (params = ${params})`);

    return this.http
        .get<T>(endpoint, { headers: this.headerOptions}).pipe(
        catchError( error => {
        console.log(error);
        this.alertService.error(error.error.message || error.message);
        return EMPTY;
      })
    );
  }

  public update(params?: Entity): Observable<T | undefined>
  {
    const endpoint = `${this.url}${this.endpoint}/${params?._id}`


    console.log(`update ${endpoint} (params = ${params})`);

    return this.http
        .put<T>(endpoint, params, {headers: this.headerOptions}).pipe(
        catchError( error => {
        console.log(error);
        this.alertService.error(error.error.message || error.message);
        return EMPTY;
      })
    );
  }

  public create(params?: Entity): Observable<T | undefined>
  {
    const endpoint = `${this.url}${this.endpoint}`

    const headerOptions = new HttpHeaders({
      'Authorization': `Bearer ${JSON.parse(this.object).entity.token}`
    });

    console.log(`create ${endpoint} (params = ${params})`);

    return this.http
        .post<T>(endpoint, params, {headers: this.headerOptions}).pipe(
        catchError( error => {
        console.log(error);
        this.alertService.error(error.error.message || error.message);
        return EMPTY;
      })
    );
  }

  public delete(params?: HttpParams, id?: string): Observable<T | undefined>
  {
    const endpoint = `${this.url}${this.endpoint}/${id}`

    console.log(`delete ${endpoint} (params = ${params})`);

    return this.http
        .delete<T>(endpoint, {headers: this.headerOptions}).pipe(
        catchError( error => {
        console.log(error);
        this.alertService.error(error.error.message || error.message);
        return EMPTY;
      })
    );
  }
}
