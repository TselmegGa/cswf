import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingModule } from './loading/loading.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../auth/login/login.component';



@NgModule({

  imports: [
    CommonModule,
    LoadingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class AuthModule {}
