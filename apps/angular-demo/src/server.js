const express = require("express");
const path = require("path");
const http = require("http");
const compression = require("compression");

const app = express();

// Compress static assets to enhance performance.
// Decrease the download size of your app through gzip compression:
app.use(compression());

// Listen on provided port, on all network interfaces.

const appname = "angular-demo";

// Point static path to dist
app.use(express.static(path.join(__dirname, "..", "../../dist/apps/", appname)));

// Catch all routes and return the index file
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "..", "../../dist/apps/", appname, "index.html"));
});

// Get port from environment and store in Express.
const port = process.env.PORT || "4200";
app.set("port", port);
// Create HTTP server.
const server = http.createServer(app);

// Listen on provided port, on all network interfaces.
server.listen(port, () => {
  console.log(
    `Angular app \'${appname}\' running in ${process.env.NODE_ENV} mode on port ${port}`
  );
});
